FROM alpine:latest

RUN apk add --no-cache python3 git

COPY check-dco.py /usr/bin/check-dco

ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

CMD [ "/usr/bin/check-dco" ]
