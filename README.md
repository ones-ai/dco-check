# DCO check

[![Pipeline Status](https://gitlab.com/ones-ai/dco-check/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/ones-ai/dco-check/-/commits/master)
[![Release Status](https://gitlab.com/ones-ai/dco-check/-/badges/release.svg)](https://gitlab.com/ones-ai/dco-check/-/releases)
[![Documentation](https://img.shields.io/badge/documentation-Wiki-blue)](https://gitlab.com/ones-ai/dco-check/-/wikis/home)
[![Docker Hub](https://img.shields.io/badge/docker-images-lightblue)](https://hub.docker.com/r/onesai1/dco-check/tags)

This is a docker project to build and publish container images for dco-check.
